import * as React from "react";
import AddTodo from "./components/AddTodo";
import VisibleTodoList from "./components/VisibleTodoList";
import Footer from "./components/Footer";


const Todo = () => <div className="container">
    <div className="row" style={{marginTop: 20}}>
        <div className="col-6">
            <AddTodo/>
            <Footer/>
        </div>
        <div className="col-6">
            <VisibleTodoList/>
        </div>
    </div>
</div>;

export default Todo;