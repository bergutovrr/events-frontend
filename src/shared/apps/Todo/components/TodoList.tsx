import * as React from 'react';
import List from "../../../components/atoms/List";
import TodoItem from "../../../models/TodoItem";
import ListItem from "../../../components/atoms/ListItem";

type TodoListItemType = new () => ListItem<TodoItem>;
const TodoListItem = ListItem as TodoListItemType;

interface Props {
    todoItems: TodoItem[],
    onTodoClick: (index: number) => void
}

const TodoList = (props: Props) => {

    if (props.todoItems.length <= 0) return <div className="alert alert-success" role="alert">
        <h4 className="alert-heading">Well done!</h4>
        <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer
            so that you can see how spacing within an alert works with this kind of content.</p>
        <hr/>
        <p className="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
    </div>;

    return <List>
        {props.todoItems.map((todo, i) => {
            return <TodoListItem active={todo.completed} object={todo} key={i} onClick={() => props.onTodoClick(i)}>
                {todo.text}
            </TodoListItem>
        })}
    </List>
};

export default TodoList;