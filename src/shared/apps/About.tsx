import * as React from 'react';

export default class About extends React.Component {

	render() {
		return <div className="container">
			<div className="row">
				<div className="col-12 col-sm-12 col-md-6 col-lg-6">

				</div>
				<div className="col-12 col-sm-12 col-md-6 col-lg-6">
					<form style={{marginTop: '10px'}}>
						<div className="form-group">
							<label>Email</label>
							<input type="email" className="form-control" placeholder="Email"/>
						</div>
						<div className="form-group">
							<label>Password</label>
							<input type="password" className="form-control" placeholder="Password"/>
						</div>
						<div className="form-group">
							<label>Address</label>
							<input type="text" className="form-control" placeholder="1234 Main St"/>
						</div>
						<div className="form-group">
							<label>Address 2</label>
							<input type="text" className="form-control" placeholder="Apartment, studio, or floor"/>
						</div>
						<div className="form-row">
							<div className="form-group col-md-6">
								<label>City</label>
								<input type="text" className="form-control"/>
							</div>
							<div className="form-group col-md-4">
								<label>State</label>
								<select className="form-control" defaultValue="def">
									<option value="def">Choose...</option>
									<option>...</option>
								</select>
							</div>
							<div className="form-group col-md-2">
								<label>Zip</label>
								<input type="text" className="form-control"/>
							</div>
						</div>
						<div className="form-group">
							<div className="form-check">
								<input className="form-check-input" type="checkbox"/>
								<label className="form-check-label">
									Check me out
								</label>
							</div>
						</div>
						<button type="submit" className="btn btn-primary">Sign in</button>
					</form>
				</div>
			</div>
		</div>
	}
}