import * as React from 'react';
import Endpoint from "../../../layers/api/api";
import Form, {Inputs} from "./Form";
import {default as Button, Type} from "../../atoms/Button";
import request from "../../../layers/http";
import StringInput from "./fields/StringInput";
import NumberInput from "./fields/NumberInput";
import Spinner from "../../atoms/Spinner";

interface Props<T> {
    endpoint: Endpoint,
    onChange?: (inputs: Inputs<Nullable<T>>) => any,
    onSubmit?: (inputs: Inputs<Nullable<T>>) => any
}

interface Input {
    type: "string" | "integer" | "datetime" | "field",
    required: boolean,
    read_only: boolean,
    label: string,

    max_length?: number
    help_text?: string,
}

interface FormDescription<T> {
    name: string,
    description: string,
    renders: string[],
    parses: string[],
    actions?: {[property in "POST" | "PUT"]: {[p in keyof T]: Input}}
}

interface State<T> {
    isLoading: boolean,
    description?: FormDescription<T>
}

type RemotelyBuiltFormType<T> = new () => Form<T>;

type Nullable<T> = {
    [p in keyof T]: T[keyof T] | null
}

export default class RemotelyBuiltForm<T> extends React.Component<Props<T>, State<T>> {

    constructor(props: Props<T>) {
        super(props);
        this.state = {
            isLoading: true
        }
    }

    load = async () => {
        const response = await request(this.props.endpoint.toString(), {
            method: "OPTIONS"
        });

        this.setState({
            description: await response.json(),
            isLoading: false
        })
    };

    render() {

        const CastedRemotelyBuiltForm = Form as RemotelyBuiltFormType<Nullable<T>>;

        return <CastedRemotelyBuiltForm onChange={this.props.onChange} onSubmit={this.props.onSubmit}>
            {(onChange: (field: keyof Nullable<T>, value: Nullable<T>[keyof Nullable<T>]) => any) => {

                const inputs = [];

                const {description} = this.state;
                if (!description) return <Spinner/>;
                if (!description.actions) return <span/>;
                if (!description.actions.POST) return <span/>;

                for (let i in description.actions.POST) {
                    if (!description.actions.POST.hasOwnProperty(i)) continue;

                    const item: Input = description.actions.POST[i];

                    switch (item.type) {
                        case "string":
                            inputs.push(<StringInput key={i} name={i} onChange={value => {
                                onChange(i, value as any);
                            }} label={item.label} helpText={item.help_text}/>);
                            break;
                        case "integer":
                            inputs.push(<NumberInput key={i} label={i} name={i} onChange={value => {
                                onChange(i, value as any);
                            }} helpText={item.help_text}/>);
                            break;
                    }
                }

                return [
                    ...inputs,
                    <Button label="Зарегистрироваться" key="register-button" type={Type.Submit} block={true}/>
                ]
            }}
        </CastedRemotelyBuiltForm>
    }

}