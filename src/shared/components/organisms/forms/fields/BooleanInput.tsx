import {BasicInput} from "./index";
import * as React from 'react';

type Props = BasicInput<boolean>;

const BooleanInput = (props: Props) => {
    return <input checked={props.value === null ? false : props.value} type="checkbox"/>;
};

export default BooleanInput;