export interface BasicInput<T> {
    label: string,
    name: string,

    value?: T | null,
    required?: boolean,
    disabled?: boolean,
    helpText?: string,
    hidden?: boolean
    onChange?: (value: T | null) => any
}

export interface TextInput<T> extends BasicInput<T> {
    placeholder?: string
}