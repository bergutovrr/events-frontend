import * as React from "react";
import {ErrorInfo, FormEvent} from "react";
import Button, {Type} from "../../atoms/Button";

export type Inputs<T> = {[property in keyof T]?: T[property]};

export type Errors<T> = {[property in keyof T]?: string[]};

interface Props<T> {
    children: (onChange: (field: keyof T, value: T[keyof T]) => any) => React.ReactNode,
    onChange?: (inputs: Inputs<T>) => any,
    onSubmit?: (inputs: Inputs<T>) => any
}


interface State<T> {
    inputs: Inputs<T>,
    errors: Errors<T>
}

export default class Form<T> extends React.Component<Props<T>, State<T>> {

    state: State<T> = {
        inputs: {},
        errors: {}
    };

    onChange = (field: keyof T, value: T[keyof T]) => {

        const state: State<T> = this.state;
        const inputs: Inputs<T> = state.inputs;
        const errors: Errors<T> = state.errors;

        inputs[field] = value;
        delete errors[field];

        if (this.props.onChange) this.props.onChange(inputs);

        this.setState({inputs: inputs, errors: errors});
    };

    onSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (this.props.onSubmit) this.props.onSubmit(this.state.inputs);
        return false;
    };

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error(error, errorInfo);
    }

    render() {
        return <form action="" onSubmit={this.onSubmit}>
            {this.props.children(this.onChange)}
        </form>;
    }
}