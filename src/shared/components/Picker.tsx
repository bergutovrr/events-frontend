import * as React from 'react';

interface Props {
	options: string[],
	value: string,
	onChange: (value: string) => void
}


export default class Picker extends React.Component<Props> {
	render() {
		const {value, onChange, options} = this.props;

		return <span>
			<h1>{value}</h1>
			<select onChange={e => onChange(e.target.value)}>
				{options.map(option => (
					<option value={option} key={option}>
						{option}
					</option>
				))}
			</select>
		</span>
	}
}
