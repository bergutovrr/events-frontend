import * as React from 'react';

const Spinner = () => {
    return <div className="loader">Загрузка...</div>;
};

export default Spinner;