import {VisibilityFilter} from "../../models/TodoItem";
import {ActionType, RootAction} from "./index";


export interface AddTodoAction extends RootAction {
    type: ActionType.AddTodo,
    text: string
}

export interface ToggleTodoAction extends RootAction {
    type: ActionType.ToggleTodo,
    index: number
}

export interface SetVisibilityFilterAction extends RootAction {
    type: ActionType.SetVisibilityFilter,
    filter: VisibilityFilter
}

export function addTodo(text: string): AddTodoAction {
    return {type: ActionType.AddTodo, text}
}

export function toggleTodo(index: number): ToggleTodoAction {
    return {type: ActionType.ToggleTodo, index};
}

export function setVisibilityFilter(filter: VisibilityFilter): SetVisibilityFilterAction {
    return {type: ActionType.SetVisibilityFilter, filter};
}