import {Dispatch} from "redux";
import AppAction from "../actions";

interface Params {
    dispatch: Dispatch
}

const promiseMiddleware = ({dispatch}: Params) => {
    return (next: Dispatch) => async (action: AppAction | Promise<any>) => {
        if (action instanceof Promise) action.then(dispatch);
        else next(action);
    }
};

export default promiseMiddleware;