import {ActionType, default as AppAction} from "../actions";
import User from "../../models/User";

export function user(state: User | null = null, action: AppAction): User | null {
    switch (action.type) {
        case ActionType.Login:
            return action.user;
        case ActionType.Logout:
            return null;
        default:
            return state;
    }
}