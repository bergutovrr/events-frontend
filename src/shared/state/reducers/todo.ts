import TodoItem, {VisibilityFilter} from "../../models/TodoItem";
import {ActionType, default as AppAction} from "../actions";


export function visibilityFilter(state = VisibilityFilter.ShowAll, action: AppAction): VisibilityFilter {
    switch (action.type) {
        case ActionType.SetVisibilityFilter:
            return action.filter;
        default:
            return state;
    }
}

export function todoItems(state: TodoItem[] = [], action: AppAction): TodoItem[] {
    switch (action.type) {
        case ActionType.AddTodo:
            return [
                ...state,
                {
                    text: action.text,
                    completed: false
                }
            ];
        case ActionType.ToggleTodo:
            return state.map((todo, i) => {
                if (i === action.index) return {...todo, completed: !todo.completed};

                return todo;
            });

        default:
            return state;
    }
}