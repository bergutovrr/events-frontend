import {combineReducers} from "redux";
import RootState from "../RootState";
import {todoItems, visibilityFilter} from "./todo";
import AppAction from "../actions";
import {user} from "./user";

const reducer = combineReducers<RootState, AppAction>({
    visibilityFilter,
    todoItems,
    user
});

export default reducer;