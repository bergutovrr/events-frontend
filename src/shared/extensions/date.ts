declare global {
    interface Date {
        difference(date: Date, interval: Interval): number;
    }
}

export enum Interval {
    second,
    minute,
    hour,
    day,
    week,
    month,
    year
}

Date.prototype.difference = function (date: Date, interval: Interval) {
    switch (interval) {
        case Interval.year:
            return date.getFullYear() - this.getFullYear();
        case Interval.month:
            return (date.getMonth() + 12 * date.getFullYear()) - (this.getMonth() + 12 * this.getFullYear());
        case Interval.week:
            return ((date.getTime() - this.getTime()) / (24 * 3600 * 1000 * 7));
        case Interval.day:
            return ((date.getTime() - this.getTime()) / (24 * 3600 * 1000));
        case Interval.hour:
            return ((date.getTime() - this.getTime()) / (24 * 3600));
        case Interval.minute:
            return (date.getTime() - this.getTime()) / 24;
        case Interval.second:
            return date.getSeconds() - this.getSeconds();
        default:
            return 1;
    }
};