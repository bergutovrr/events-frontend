import RootState from "../state/RootState";
import {PublicEnvironment} from "../layers/environment";

export default interface Window {
    __ENVIRONMENT__: PublicEnvironment,
    __PRELOADED_STATE__?: string | RootState,
    __REDUX_DEVTOOLS_EXTENSION__?: any,
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any
}