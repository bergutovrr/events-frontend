import * as React from "react";
import {Route, Switch} from "react-router";
import routes from './routes';
import {hot} from 'react-hot-loader';
import {Link} from "react-router-dom";


const App = () => {
    return <div>
        <nav className="navbar navbar-light bg-light">
            <div className="container">
                <Link className="navbar-brand" to="/">EVENTS</Link>

                <ul className="navbar-nav mr-auto">
                    {routes
                        .filter(route => route.path !== '/')
                        .map(route => <li key={route.path} className="nav-item active">
                            <Link className="nav-link" to={route.path ? route.path : '/'}>{route.title}<span
                                className="sr-only">(current)</span></Link>
                        </li>)}
                </ul>
                <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>

        <Switch>
            {routes.map(route => <Route key={route.path} {...route}/>)}
        </Switch>
    </div>
};

export default hot(module)(App);
