const request = async (input?: Request | string, init?: RequestInit): Promise<Response> => {
    if (!init) init = {};
    if (!init.credentials) init.credentials = "include";
    return fetch(input, init);
};

export default request;