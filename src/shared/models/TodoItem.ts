export default interface TodoItem {
    text: string,
    completed: boolean
}

export enum VisibilityFilter {
    ShowAll,
    ShowCompleted,
    ShowActive
}