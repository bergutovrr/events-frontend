import Model from "./Model";

export default interface Service extends Model {
    price: string;
    category: { id: string, title: string }
}