import * as express from 'express';
import * as React from 'react';
import * as dotenv from "dotenv";
import {initialHandler} from "./shared";

import * as webpack from 'webpack';
import * as devConfig from '../../webpack.development';
import * as devMiddleware from 'webpack-dev-middleware';
import * as hotMiddleware from 'webpack-hot-middleware';
import {publicEnvironment} from "./layers/environment";

// dotenv.config();

// global.ENVIRONMENT = publicEnvironment;

const app = express();

const compiler = webpack(devConfig.default);

const publicPath = devConfig.default.output && devConfig.default.output.publicPath
    ? devConfig.default.output.publicPath
    : "/";

app.get('/', initialHandler);

app.use(devMiddleware(compiler, {
    publicPath
}));

app.use(hotMiddleware(compiler));

app.get('*', initialHandler);

const PORT = 3000;

app.listen(PORT, (error: Error) => {
    console.log(`http://localhost:${PORT}`);

    if (error) return console.error(error.message);
});