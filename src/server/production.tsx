import * as express from 'express';
import * as React from 'react';
import * as compression from 'compression';
import {Request} from "express";
import {initialHandler} from './shared';
import * as path from "path";
import * as dotenv from "dotenv";
import {publicEnvironment} from "./layers/environment";

dotenv.config();

global.ENVIRONMENT = publicEnvironment;

const app = express();

app.use(express.static(path.resolve('build/client')));
app.use(compression({filter: shouldCompress}));

app.get('*', initialHandler);
const PORT = 3000;


app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
});

function shouldCompress(req: Request) {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false;
    }

    // fallback to standard filter function
    return true;
}


