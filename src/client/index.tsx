import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from "../shared/App";
import {BrowserRouter} from "react-router-dom";

import '../styles/style.scss';
import store from "../shared/state/store";
import {Provider} from "react-redux";

const render = () => {
	const root = document.getElementById('root'),
		node = <Provider store={store}>
			<BrowserRouter>
				<App/>
			</BrowserRouter>
		</Provider>;
	ReactDOM.hydrate(node, root);
};

render();