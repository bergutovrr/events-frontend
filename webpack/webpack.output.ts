import {Output} from "webpack";
import * as config from "../config";

const output: Output = {
    path: config.clientBuild,
    filename: config.bundleTitle,
    publicPath: config.staticPath
};

export default output;